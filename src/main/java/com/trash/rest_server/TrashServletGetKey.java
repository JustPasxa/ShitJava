package com.trash.rest_server;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.UUID;
import java.util.Date;



public class TrashServletGetKey extends HttpServlet{
    private static final String url = "jdbc:mysql://localhost:3306/test";
    private static final String user = "root";
    private static final String password = "root";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // UUID k = new UUID():

        Date date = new Date();
        UUID one = UUID.randomUUID();
        String query = "INSERT INTO test.books (id, name, author) \n" +
                       "VALUES (one, 'выдан', date.toString());";

        try {
            con = java.sql.DriverManager.getConnection(url, user, password);
            stmt = con.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException sql_ex){
            sql_ex.printStackTrace();
        }finally {
            try { con.close(); } catch(SQLException se) { /* pass  */ }
            try { stmt.close(); } catch(SQLException se) { /* pass  */  }
            try { rs.close(); } catch(SQLException se) { /* pass  */ }
        }
        PrintWriter out = resp.getWriter();
        out.print(one);
    }
}
